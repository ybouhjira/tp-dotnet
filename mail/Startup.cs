﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(mail.Startup))]
namespace mail
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
