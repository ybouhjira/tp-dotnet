﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="mail.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        .error {
            font-weight: bold;
            color: red;
        }
         body {
             padding-top: 60px;
         }

        .form-control {
            border-radius: 0;
        }

        .form-container {
            padding: 30px;
            border-radius: 0;
        }

        .main-title {
            text-align: center;
        }

        .control-label {
            font-weight: bold;
        }

        .name-input {
            padding: 0;
        }

        .name-input:first-child {
            padding-right: 2%;
        }

        .name-input:nth-child(2) {
            padding-left: 2%;
        }

        .col-sm-12 .form-control, select {
            width: 100% !important;
            max-width: none;
        }

        .date-inputs {
            padding: 0;
        }

        input[type="checkbox"] + label {
            font-weight: normal;
        }

        * {
            border-radius: 0 !important;
        }
    </style>

    <h1 class="main-title">Create your account
    </h1>

    <div class="container body-content form form-horizontal">

        <div class="col-md-8"></div>

        <div class="form-container well col-md-4">

            <div class="form-group">
                <asp:Label ID="FirstNameLabel" runat="server" Text="Name" CssClass="col-sm-12 control-label"></asp:Label>
                <div class="col-sm-12">
                    <div class="col-sm-6 name-input">
                        <asp:TextBox ID="FirstName" runat="server" CssClass="form-control" placeholder="Prénom"></asp:TextBox>
                    </div>
                    <div class="col-sm-6 name-input">
                        <asp:TextBox ID="LastName" runat="server" CssClass="form-control" placeholder="Nom"></asp:TextBox>
                    </div>
                </div>

                <asp:RequiredFieldValidator
                    Display="Dynamic"
                    ID="NameRequired"
                    runat="server"
                    ControlToValidate="FirstName">
                    <div class="error">
                        Le nom est requis
                    </div>
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator
                    Display="Dynamic"
                    ID="FirstNameValid"
                    runat="server"
                    ControlToValidate="FirstName"
                    ValidationExpression="([a-zA-Z]+ *)+">
                    <div class="error">
                        Prénom invalide
                    </div>
                </asp:RegularExpressionValidator>

                <asp:RequiredFieldValidator
                    Display="Dynamic"
                    ID="LastNameRequired"
                    runat="server"
                    ControlToValidate="LastName">
                    <div class="error">
                        Le prénom est requis
                    </div>
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator
                    Display="Dynamic"
                    ID="LastNameRegex"
                    runat="server"
                    ControlToValidate="LastName"
                    ValidationExpression="([a-zA-Z]+ *)+">
                    <div class="error">
                        Prénom invalide
                    </div>
                </asp:RegularExpressionValidator>

            </div>


            <div class="form-group">
                <asp:Label ID="UsernameLabel" runat="server" Text="Username" CssClass="col-sm-12 control-label"></asp:Label>
                <div class="col-sm-12">
                    <asp:TextBox ID="Username" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                
                <asp:RequiredFieldValidator
                    Display="Dynamic"
                    ID="UserNameReq"
                    runat="server"
                    ControlToValidate="Username">
                    <div class="error">
                        Le nom d'utilisateur est requis.
                    </div>
                </asp:RequiredFieldValidator>

                <asp:RegularExpressionValidator
                    Display="Dynamic"
                    ID="UsernameRegex"
                    runat="server"
                    ControlToValidate="FirstName"
                    ValidationExpression="/([a-z]){5,10}/">
                    <div class="error">
                        Le nom d'utilisateur doit être composé que de lettres minuscule.
                         
                    </div>
                </asp:RegularExpressionValidator>
            </div>


            <div class="form-group">
                <asp:Label ID="Label2" runat="server" Text="Password" CssClass="col-sm-12 control-label"></asp:Label>
                <div class="col-sm-12">
                    <asp:TextBox ID="Password" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                </div>
                
                <asp:RequiredFieldValidator
                    Display="Dynamic"
                    ID="PasswordReq"
                    runat="server"
                    ControlToValidate="Password">
                    <div class="error">
                        Le mot de passe est requis
                    </div>
                </asp:RequiredFieldValidator>
            </div>

            <div class="form-group">
                <asp:Label ID="ConfirmLabel" runat="server" Text="Confirm password" CssClass="col-sm-12 control-label">
                </asp:Label>
                <div class="col-sm-12">
                    <asp:TextBox ID="Confirm" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                </div>
                
                

                <asp:RequiredFieldValidator
                    Display="Dynamic"
                    ID="CompareReq"
                    runat="server"
                    ControlToValidate="Confirm">
                    <div class="error">
                        La confirmation du mot de passe est requise
                    </div>
                </asp:RequiredFieldValidator>
                <asp:CompareValidator
                    ID="CompareValidator1"
                    runat="server"
                    ControlToValidate="Confirm"
                    ControlToCompare="Password"
                    Display="Dynamic">
                        <div class='alert alert-danger'>Les mots de passes doivent êtres identiques</div>
                </asp:CompareValidator>

            </div>

            <div class="form-group date-group">
                <asp:Label ID="BirthDayLabel" runat="server" Text="Birthday" CssClass="col-sm-12 control-label"></asp:Label>

                <div class="row col-sm-12" style="padding-left: 30px; padding-right: 0">

                    <div class="col-sm-4 date-inputs" style="padding-right: 10px">
                        <asp:TextBox ID="Day" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>

                    <div class="col-sm-4 date-inputs">
                        <asp:DropDownList ID="Month" runat="server" CssClass="form-control">
                            <asp:ListItem>Janvier</asp:ListItem>
                            <asp:ListItem>Février</asp:ListItem>
                            <asp:ListItem>Mars</asp:ListItem>
                            <asp:ListItem>Avril</asp:ListItem>
                            <asp:ListItem>Mai</asp:ListItem>
                            <asp:ListItem>Juin</asp:ListItem>
                            <asp:ListItem>Juillet</asp:ListItem>
                            <asp:ListItem>Août</asp:ListItem>
                            <asp:ListItem>Septembre</asp:ListItem>
                            <asp:ListItem>Novembre</asp:ListItem>
                            <asp:ListItem>Décembre</asp:ListItem>
                        </asp:DropDownList>
                    </div>

                    <div class="col-sm-4 date-inputs" style="padding-left: 10px">
                        <asp:TextBox ID="Year" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                
                <asp:RequiredFieldValidator
                    Display="Dynamic"
                    ID="RequiredFieldValidator1"
                    runat="server"
                    ControlToValidate="Day">
                    <div class="error">
                        Le jour est requis
                    </div>
                </asp:RequiredFieldValidator>
                
                <asp:RequiredFieldValidator
                    Display="Dynamic"
                    ID="RequiredFieldValidator2"
                    runat="server"
                    ControlToValidate="Month">
                    <div class="error">
                        Le mois est requis
                    </div>
                </asp:RequiredFieldValidator>
                
                <asp:RequiredFieldValidator
                    Display="Dynamic"
                    ID="RequiredFieldValidator3"
                    runat="server"
                    ControlToValidate="Year">
                    <div class="error">
                        L'année est requise
                    </div>
                </asp:RequiredFieldValidator>
                
                <asp:CustomValidator ID="DateValidator" runat="server" ControlToValidate="Day" OnServerValidate="DateValidator_ServerValidate">
                    <div class="error">
                        Date invalide
                    </div>
                </asp:CustomValidator>
            </div>

            <div class="form-group">
                <asp:Label ID="GenderLabel" runat="server" Text="Sexe" CssClass="col-sm-12 control-label"></asp:Label>
                <div class="col-sm-12 ">
                    <asp:DropDownList ID="Gender" runat="server" CssClass="form-control">
                        <asp:ListItem>Homme</asp:ListItem>
                        <asp:ListItem>Femme</asp:ListItem>
                    </asp:DropDownList>
                </div>
                
                
                <asp:RequiredFieldValidator
                    Display="Dynamic"
                    ID="RequiredFieldValidator4"
                    runat="server"
                    ControlToValidate="Gender">
                    <div class="error">
                        Le sexe est requis
                    </div>
                </asp:RequiredFieldValidator>
            </div>


            <div class="form-group">
                <asp:Label ID="CountryLabel" runat="server" Text="Pays" CssClass="col-sm-12 control-label"></asp:Label>

                <div class="col-sm-12 ">
                    <asp:DropDownList ID="Country" runat="server" CssClass="form-control">
                        <asp:ListItem>Maroc</asp:ListItem>
                        <asp:ListItem>Tunisie</asp:ListItem>
                        <asp:ListItem>Algérie</asp:ListItem>
                        <asp:ListItem>Espagne</asp:ListItem>
                        <asp:ListItem>Etats-Unis</asp:ListItem>
                        <asp:ListItem>Japon</asp:ListItem>
                        <asp:ListItem>Australie</asp:ListItem>
                        <asp:ListItem>France</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>

            <div class="form-group">
                <asp:Label ID="EmailLabel" runat="server" Text="Adresse e-mail actuelle" CssClass="col-sm-12 control-label"></asp:Label>
                <div class="col-sm-12">
                    <asp:TextBox ID="Email" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <asp:RegularExpressionValidator Display="Dynamic" ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="Email">
                    <div class="error">
                        Entrez une adresse email valide
                    </div>
                </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="emailRequired" runat="server" ControlToValidate="Email">
                    <div class="error">
                        L'email est requis
                    </div>
                </asp:RequiredFieldValidator>

            </div>

            <div class="form-group">
                <asp:CheckBox runat="server" Text="J'accepte les termes d'utilisation" Style="margin-left: 10px" />
            </div>

            <asp:Button ID="Button1" runat="server" Text="OK" CssClass="btn btn-primary pull-right" />

        </div>
    </div>
</asp:Content>
