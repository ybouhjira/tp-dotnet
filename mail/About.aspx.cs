﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mail
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void DateValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            var dateString = $"{this.Day.Text}/{this.Month.Text}/{this.Year.Text}";
            DateTime dateTime;

            args.IsValid = DateTime.TryParse(dateString, out dateTime);
        }
    }
}